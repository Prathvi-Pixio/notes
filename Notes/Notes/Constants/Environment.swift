//
//  Environment.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation

enum Server {
    
    case developement
    case staging
    case production
    case mock
}

struct Environment {
    
    // Set it to true before releasing.
    static let release:Bool     =   true
    
    static let server:Server    =   .developement
    
    // To print the log set true.
    static let debug:Bool       =   true
    
    // true for beta phase.
    static let isBeta           =   true
}
