//
//  Parameters.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation

enum Params:String {
    
    case status
    case success
    case message
    case code
    case data
    case meta    
    case errors
    case notes
    
    case id
    case title
    case content
}
