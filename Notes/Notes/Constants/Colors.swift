//
//  Colors.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    
    static let white: UIColor = UIColor.white
    static let red: UIColor = UIColor.red
    static let green: UIColor = UIColor.green
    static let blue: UIColor = UIColor.blue
}
