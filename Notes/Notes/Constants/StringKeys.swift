//
//  StringKeys.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation

public enum StringKeys:String {
    
    case genericError
    case internetConnectionOffline
    case internetConnectionSlow
    case notFoundInStoryboard
}
