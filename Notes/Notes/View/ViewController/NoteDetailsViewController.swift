//
//  NoteDetailsViewController.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import UIKit

class NoteDetailsViewController: BaseViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    var viewModel:NoteDetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup(with: viewModel)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension NoteDetailsViewController {
    
    private func setup(with viewModel: NoteDetailsViewModel) -> Void {
        setupUI(with: viewModel)
        setupBindings(for: viewModel)
    }
    
    private func setupUI(with viewModel: NoteDetailsViewModel) -> Void {
        titleTextField.text = viewModel.note.title
        contentTextView.text = viewModel.note.content
    }
    
    func setupBindings(for viewModel: NoteDetailsViewModel) -> Void {
        
        titleTextField.rx
            .text
            .bind(to: viewModel.title)
            .disposed(by: disposeBag)
        
        contentTextView.rx
            .text
            .bind(to: viewModel.content)
            .disposed(by: disposeBag)
        
        updateButton.rx.tap
            .bind(to: viewModel.updateNoteTaps)
            .disposed(by: disposeBag)
        
        deleteButton.rx.tap
            .bind(to: viewModel.deleteNoteTaps)
            .disposed(by: disposeBag)
    }
}
