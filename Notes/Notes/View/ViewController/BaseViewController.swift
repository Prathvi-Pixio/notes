//
//  BaseViewController.swift
//  Notes
//
//  Created by Prathvi Raj on 12/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BaseViewController: UIViewController {

    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

