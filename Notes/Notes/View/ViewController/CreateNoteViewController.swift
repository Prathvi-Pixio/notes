//
//  CreateNoteViewController.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import UIKit

class CreateNoteViewController: BaseViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    
    @IBOutlet weak var updateButton: UIButton!
    
    var viewModel:CreateNoteViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup(with: viewModel)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        
    }
}

extension CreateNoteViewController {
    
    private func setup(with viewModel: CreateNoteViewModel) -> Void {
        setupUI(with: viewModel)
        setupBindings(for: viewModel)
    }
    
    private func setupUI(with viewModel: CreateNoteViewModel) -> Void {
        
    }
    
    func setupBindings(for viewModel: CreateNoteViewModel) -> Void {
        
        titleTextField.rx
            .text
            .bind(to: viewModel.title)
            .disposed(by: disposeBag)
        
        contentTextView.rx
            .text
            .bind(to: viewModel.content)
            .disposed(by: disposeBag)
        
        
        updateButton.rx.tap
            .bind(to: viewModel.updateNoteTaps)
            .disposed(by: disposeBag)
    }
}
