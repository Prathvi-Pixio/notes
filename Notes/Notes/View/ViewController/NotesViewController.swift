//
//  NotesViewController.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import UIKit

class NotesViewController: BaseViewController {

    @IBOutlet weak var createNoteButton: UIBarButtonItem!
    @IBOutlet weak var notesTableview: UITableView!
    
    var viewModel:NotesViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup(with: viewModel)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        
    }
}

extension NotesViewController {
    
    private func setup(with viewModel: NotesViewModel) -> Void {
        setupUI(with: viewModel)
        setupBindings(for: viewModel)
    }
    
    private func setupUI(with viewModel: NotesViewModel) -> Void {
        
    }
    
    func setupBindings(for viewModel: NotesViewModel) -> Void {
        
        createNoteButton.rx.tap
            .bind(to: viewModel.addNotesTaps)
            .disposed(by: disposeBag)
        
        notesTableview.rx.itemSelected
            .bind(to: viewModel.selectedIndexPath)
            .disposed(by: disposeBag)
        
        notesTableview.rx.itemSelected.asObservable().map { (index) -> Notes in
            return viewModel.notes.value[index.row]
        }.bind(to: viewModel.selectedItem)
            .disposed(by: disposeBag)
        
        viewModel.notes.asObservable().subscribe(onNext: { (notes) in
            self.notesTableview.reloadData()
        }).disposed(by: disposeBag)
    }
}

extension NotesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension NotesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.notes.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NotesTableViewCell.self), for: indexPath) as! NotesTableViewCell
        cell.configure(data: viewModel.notes.value[indexPath.row])
        return cell
    }
}
