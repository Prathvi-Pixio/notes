//
//  Notes.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation

class Notes: Codable {
    
    var id: String?
    var title: String?
    var content: String?
    
    init(id: String? = nil, title: String? = nil, content: String? = nil) {
        self.id = id
        self.title = title
        self.content = content
    }
}
