//
//  NotesResponse.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation

class NotesResponse: BaseResponse {
    var notes: [Notes] = []
    
    override init(response: [String : Any]?) {
        super.init(response: response)
        
        guard success else {
            return
        }
        
        guard let data = self.data else {
            return
        }
        
        guard let notesDictArray = data[Params.notes.rawValue] as? [[String:Any]] else {
            return
        }
        
        for notesDict in notesDictArray {
            
            if let notesData = try? JSONSerialization.data(withJSONObject: notesDict, options: []) {
                
                if let note = try? JSONDecoder().decode(Notes.self, from: notesData) {
                    notes.append(note)
                }
            }
        }
    }
}
