//
//  BaseResponse.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation
class BaseResponse {
    
    var success: Bool = false
    var message: String?
    var data: [String:Any]?
    var meta: [String: Any]?
    var errors: [[String: Any]]?
    
    init(response:[String:Any]?) {
        
        guard let response = response else { return  }
        
        if let success = response[Params.success.rawValue] as? Bool {
            self.success = success
        }

        if let message = response[Params.message.rawValue] as? String {
            self.message = message
        }

        if let data = response[Params.data.rawValue] as? [String:Any] {
            self.data = data
        }

        if let meta = response[Params.meta.rawValue] as? [String:Any] {
            self.meta = meta
        }

        if let errors = response[Params.errors.rawValue] as? [[String:Any]] {
            self.errors = errors
        }
    }
}
