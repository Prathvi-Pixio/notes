//
//  CreateNotesResponse.swift
//  Notes
//
//  Created by Prathvi Raj on 15/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation

class CreateNotesResponse: BaseResponse {
    
    override init(response: [String : Any]?) {
        super.init(response: response)
        
        guard success else {
            return
        }
        
        guard let data = self.data else {
            return
        }
        
        plog(data)
    }
}
