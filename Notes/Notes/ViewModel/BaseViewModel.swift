//
//  BaseViewModel.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class BaseViewModel {
    
    // Dispose Bag
    let disposeBag = DisposeBag()
}
