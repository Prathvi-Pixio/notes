//
//  NotesViewModel.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class NotesViewModel: BaseViewModel {
    typealias Dependencies = HasAPI
    
    private let dependencies: Dependencies
    
    let isLoading: ActivityIndicator =  ActivityIndicator()
    let addNotesTaps = PublishSubject<Void>()
    let selectedIndexPath = BehaviorRelay<IndexPath?>(value: nil)
    let selectedItem = BehaviorRelay<Notes?>(value: nil)
    let notes: Variable<[Notes]> = Variable([])
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init()
        
        readNotes()
    }
    
    deinit {
        
    }
}

// MARK: Networking
extension NotesViewModel {
    
    private func readNotes() -> Void {
        
        dependencies.api.readNotes().map { (result) -> [Notes] in
            switch result {
            case .success(let response):
                return response.notes
            default:
                return []
            }
        }.bind(to: notes)
            .disposed(by: disposeBag)
    }
}



