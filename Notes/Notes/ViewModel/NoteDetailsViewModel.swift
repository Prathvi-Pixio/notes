//
//  NoteDetailsViewModel.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum NotesDetailsResult {
    case none
    case deleted
    case updated(note: Notes)
}

class NoteDetailsViewModel: BaseViewModel {
    typealias Dependencies = HasAPI
    
    private let dependencies: Dependencies
    
    let isLoading: ActivityIndicator =  ActivityIndicator()
    
    let updateNoteTaps = PublishSubject<Void>()
    let deleteNoteTaps = PublishSubject<Void>()
    
    let title: BehaviorRelay<String?>
    let content: BehaviorRelay<String?>
    
    let notesDetailsResult = BehaviorSubject<NotesDetailsResult>(value: .none)
    
    let note: Notes
    
    init(note: Notes, dependencies: Dependencies) {
        self.dependencies = dependencies
        self.note = note
        
        self.title = BehaviorRelay(value: self.note.title)
        self.content = BehaviorRelay(value: self.note.content)
        
        super.init()
        
        updateNoteTaps.asObservable()
            .debounce(0.3, scheduler: MainScheduler.instance)
            .subscribe(onNext: { () in
                self.updateNotes()
            }).disposed(by: disposeBag)
        
        deleteNoteTaps.asObservable()
            .debounce(0.3, scheduler: MainScheduler.instance)
            .subscribe(onNext: { () in
                self.deleteNotes()
            }).disposed(by: disposeBag)
    }
    
    deinit {
        
    }
}

// MARK: Networking
extension NoteDetailsViewModel {
    
    private func updateNotes() -> Void {
        
        guard let noteId = note.id, let titleString = title.value, let contentString = content.value else {
            return
        }
        
        dependencies.api.updateNote(id: noteId, title: titleString, content: contentString)
            .subscribe(onNext: { (result) in
                self.note.title = titleString
                self.note.content = contentString
                self.notesDetailsResult.onNext(.updated(note: self.note))
            }).disposed(by: disposeBag)
    }
    
    private func deleteNotes() -> Void {
        
        guard let noteId = note.id else {
            return
        }
        
        dependencies.api.deleteNote(id: noteId)
            .subscribe(onNext: { (result) in
                self.notesDetailsResult.onNext(.deleted)
            }).disposed(by: disposeBag)
    }
}
