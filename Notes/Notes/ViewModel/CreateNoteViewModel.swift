//
//  CreateNoteViewModel.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum CreateNoteResult {
    case none
    case note(note: Notes)
}

class CreateNoteViewModel: BaseViewModel {
    typealias Dependencies = HasAPI
    
    private let dependencies: Dependencies
    
    let updateNoteTaps = PublishSubject<Void>()
    let title = BehaviorRelay<String?>(value: nil)
    let content = BehaviorRelay<String?>(value: nil)
    
    let createNoteResult = BehaviorSubject<CreateNoteResult>(value: .none)
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init()
        
        updateNoteTaps.asObservable()
            .debounce(0.3, scheduler: MainScheduler.instance)
            .subscribe(onNext: { () in
                self.createNotes()
            }).disposed(by: disposeBag)
    }
    
    deinit {
        
    }
}

// MARK: Networking
extension CreateNoteViewModel {
    
    private func createNotes() -> Void {
        
        guard let titleString = title.value, let contentString = content.value else {
            return
        }
        
        dependencies.api.createNote(title: titleString, content: contentString)
            .subscribe(onNext: { (result) in
                self.createNoteResult.onNext(.note(note: Notes(id: String.random(), title: titleString, content: contentString)))
            }).disposed(by: disposeBag)
    }
}

extension String {
    
    static func random(length: Int = 8) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}
