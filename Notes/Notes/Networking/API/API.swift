//
//  API.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire

class API {
    
    init() {
        
    }
    
    func readNotes() -> Observable<APIResult<NotesResponse>> {
        
        return handleDataRequest(dataRequest: APIManager.shared.requestObservable(api: APIRouter.readNotes)).map({ (response) -> APIResult<NotesResponse> in
            
            let apiResponse = NotesResponse(response: response)
            let (apiStatus, apiCallError) = self.isAPISuccessful(response: apiResponse)
            if apiStatus { return APIResult.success(value: apiResponse) } else { return APIResult.failure(error: apiCallError) }
        })
    }
    
    func createNote(title: String, content: String) -> Observable<APIResult<CreateNotesResponse>> {
        
        return handleDataRequest(dataRequest: APIManager.shared.requestObservable(api: APIRouter.createNote(title: title, content: content))).map({ (response) -> APIResult<CreateNotesResponse> in
            
            let apiResponse = CreateNotesResponse(response: response)
            let (apiStatus, apiCallError) = self.isAPISuccessful(response: apiResponse)
            if apiStatus { return APIResult.success(value: apiResponse) } else { return APIResult.failure(error: apiCallError) }
        })
    }

    func updateNote(id: String, title: String, content: String) -> Observable<APIResult<UpdateNotesResponse>> {
        
        return handleDataRequest(dataRequest: APIManager.shared.requestObservable(api: APIRouter.updateNote(id: id, title: title, content: content))).map({ (response) -> APIResult<UpdateNotesResponse> in
            
            let apiResponse = UpdateNotesResponse(response: response)
            let (apiStatus, apiCallError) = self.isAPISuccessful(response: apiResponse)
            if apiStatus { return APIResult.success(value: apiResponse) } else { return APIResult.failure(error: apiCallError) }
        })
    }
    
    func deleteNote(id: String) -> Observable<APIResult<DeleteNotesResponse>> {
        
        return handleDataRequest(dataRequest: APIManager.shared.requestObservable(api: APIRouter.deleteNote(id: id))).map({ (response) -> APIResult<DeleteNotesResponse> in
            
            let apiResponse = DeleteNotesResponse(response: response)
            let (apiStatus, apiCallError) = self.isAPISuccessful(response: apiResponse)
            if apiStatus { return APIResult.success(value: apiResponse) } else { return APIResult.failure(error: apiCallError) }
        })
    }
    
    private func handleDataRequest(dataRequest: Observable<DataRequest>) -> Observable<[String:Any]?> {
        
        return Observable<[String: Any]?>.create({ (observer) -> Disposable in
            dataRequest.observeOn(MainScheduler.instance).subscribe({ (event) in
                switch event {
                
                case .next(let e):
                    
                    print(e.debugDescription)
                    
                    e.responseJSON(completionHandler: { (dataResponse) in
                        
                        print(dataResponse.response)
                        
                        switch dataResponse.result {
                            
                        case .success(let data):
                            
                            guard let json = data as? [String:Any] else {
                                observer.onNext(nil)
                                return
                            }
                            
                            print(json)
                            
                            observer.onNext(json)
                            
                        case .failure(let error):
                            plog(error)
                            observer.onNext(nil)
                        }
                    })
                    
                case .error(let error):
                    plog(error)
                    observer.onNext(nil)
                    
                case .completed:
                    observer.onCompleted()
                }
            })
        })
    }
    
    private func isAPISuccessful(response:BaseResponse) -> (Bool, APICallError) {
        
        guard response.success else {
            
            var message:String = pString(.genericError)
            
            if let errorMessage = response.message {
                message = errorMessage
            }
            
            return (false, APICallError(critical: false, code: 1111, reason: message, message: message))
        }
        
        return (true, APICallError.init(status: .success))
    }
}
