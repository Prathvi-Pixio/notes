//
//  APIRouter.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation
import Alamofire

// Returns URLRequestConvertible for the creating the Request object.

/*
 For Adding new API call add a case to APIRouter enum.
 Inside asURLRequest method provide
 HTTPMethod : get or post.
 params: Parameters for the API call.
 url: Base url for the request.,
 relativePath: Endpoint for the API call.
 encoding: Encoding for the parameters.
 */

enum APIRouter:URLRequestConvertible {
    
    case readNotes
    case createNote(title: String, content: String)
    case updateNote(id: String, title: String, content: String)
    case deleteNote(id: String)
    
    func asURLRequest() throws -> URLRequest {
        
        var method: HTTPMethod {
            switch self {
            case .createNote, .deleteNote, .updateNote:
                return .post
            case .readNotes:
                return .get
            }
        }
        
        let params: ([String: Any]?) = {
            switch self {
            case .createNote(let title, let content):
                return [Params.title.rawValue: title, Params.content.rawValue: content]
            case .updateNote(let id, let title, let content):
                return [Params.id.rawValue: id, Params.title.rawValue: title, Params.content.rawValue: content]
            case .deleteNote(let id):
                return [Params.id.rawValue: id]
            default:
                return nil
            }
        }()

        let url: URL = {
            
            // Add base url for the request
            let baseURL:String = {
                switch self {
                default:
                    return "http://demo7927795.mockable.io/"
                }
            }()
            
            let apiVersion: String? = {
                switch self {
                default:
                    return nil
                }
            }()
            
            // build up and return the URL for each endpoint
            let relativePath: String? = {
                switch self {
                case .createNote:
                    return "create"
                case  .updateNote:
                    return "update"
                case .deleteNote:
                    return "delete"
                case .readNotes:
                    return "notes"
                }
            }()
            
            var urlWithAPIVersion = baseURL
            
            if let apiVersion = apiVersion {
                urlWithAPIVersion = urlWithAPIVersion + apiVersion
            }
            
            var url = URL(string: urlWithAPIVersion)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            return url
        }()
        
        let encoding:ParameterEncoding = {
            
            switch self {
            default:
                return URLEncoding.default
            }
        }()
        
        let headers:[String:String]? = {
            
            let headers = ["appVersion":"1.0"]
            
            switch self {
            default:
                break
            }
            
            return headers
        }()
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers
        
        return try encoding.encode(urlRequest, with: params)
    }
}



