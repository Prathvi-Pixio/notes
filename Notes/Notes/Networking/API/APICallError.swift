//
//  APICallError.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation

class APICallError {
    
    var critical: Bool = false
    var code: Int = -1
    var reason:String = String()
    var message: String = String()
    
    init() {
        
    }
    
    init(critical:Bool, code:Int, reason:String, message:String) {
        self.critical = critical
        self.code = code
        self.reason = reason
        self.message = message
    }
    
    convenience init(status:APICallStatus) {
        switch status {
        case .success:
            self.init()
        case .failed:
            self.init(critical: false, code: 1111, reason: "Not 200.", message: pString(.genericError))
        case .forbidden:
            self.init(critical: false, code: 2222, reason: "HTTP status code 403", message: pString(.genericError))
        case .serializationFailed:
            self.init(critical: false, code: 3333, reason: "Could not parse the json", message: pString(.genericError))
        case .offline:
            self.init(critical: false, code: 4444, reason: "User offline", message: pString(.internetConnectionOffline))
        case .timeout:
            self.init(critical: false, code: 5555, reason: "Slow internet connection", message: pString(.internetConnectionSlow))
        }
    }
    
}
