//
//  APICallStatus.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation

enum APICallStatus: Error {
    
    case success
    case failed
    case forbidden
    case serializationFailed
    case offline
    case timeout
}
