//
//  UIStoryboard+Controllers.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import UIKit

extension UIStoryboard {
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}

extension UIStoryboard {
    
    var notesViewController: NotesViewController {
        
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: String(describing: NotesViewController.self)) as? NotesViewController else {
            fatalError(String(describing: NotesViewController.self) + pString(.notFoundInStoryboard))
        }
        return vc
    }
    
    var createNoteViewController: CreateNoteViewController {
        
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: String(describing: CreateNoteViewController.self)) as? CreateNoteViewController else {
            fatalError(String(describing: CreateNoteViewController.self) + pString(.notFoundInStoryboard))
        }
        return vc
    }
    
    var noteDetailsViewController: NoteDetailsViewController {
        
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: String(describing: NoteDetailsViewController.self)) as? NoteDetailsViewController else {
            fatalError(String(describing: NoteDetailsViewController.self) + pString(.notFoundInStoryboard))
        }
        return vc
    }
}
