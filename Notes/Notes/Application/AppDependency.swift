//
//  AppDependency.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

// Add any dependencies here instead of any singleton class.

import UIKit

protocol HasWindow {
    var window: UIWindow { get }
}

protocol HasAPI {
    var api: API { get }
}

struct AppDependency: HasWindow, HasAPI {
    let window: UIWindow
    let api: API
    
    init(window: UIWindow) {
        self.window = window
        self.api = API()
    }
}
