//
//  NotesCoordinator.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import Foundation
import RxSwift

class NotesCoordinator: Coordinator<Void> {
    typealias Dependencies = HasWindow & HasAPI
    
    private let rootNavigationController:UINavigationController
    private let dependencies: Dependencies
    
    init(navigationController:UINavigationController, dependencies: Dependencies) {
        self.rootNavigationController = navigationController
        self.dependencies = dependencies
    }
    
    override func start() -> Observable<CoordinationResult> {
        
        let viewModel = NotesViewModel(dependencies: dependencies)
        
        let viewController = UIStoryboard.main.notesViewController
        viewController.viewModel = viewModel
        
        viewModel.addNotesTaps
            .asObservable()
            .subscribe(onNext: { () in
                self.showCreateNote()
                    .subscribe(onNext: { (result) in
                        switch result {
                        case .none:
                            break
                        case .note(let createdNote):
                            viewModel.notes.value.insert(createdNote, at: 0)
                            self.rootNavigationController.popViewController(animated: true)
                        }
                    }).disposed(by: self.disposeBag)
            }).disposed(by: disposeBag)
        
        viewModel.selectedItem
            .asObservable()
            .subscribe(onNext: { (note) in
                if let note = note {
                    self.showNoteDetails(note: note)
                        .subscribe(onNext: { result in
                            switch result {
                            case .none:
                                break
                            case .updated(let updatedNote):
                                
                                if let selectedRow = viewModel.selectedIndexPath.value?.row {
                                    viewModel.notes.value[selectedRow] = updatedNote
                                }
                                self.rootNavigationController.popViewController(animated: true)
                                
                            case .deleted:
                                if let selectedRow = viewModel.selectedIndexPath.value?.row {
                                    viewModel.notes.value.remove(at: selectedRow)
                                }
                                self.rootNavigationController.popViewController(animated: true)
                            }
                        }).disposed(by: self.disposeBag)
                }
            }).disposed(by: disposeBag)
        
        rootNavigationController.pushViewController(viewController, animated: true)
        return Observable.never()
    }
    
    private func showNoteDetails(note: Notes) -> Observable<NotesDetailsResult> {
        let viewModel = NoteDetailsViewModel(note: note, dependencies: dependencies)
        let viewController = UIStoryboard.main.noteDetailsViewController
        viewController.viewModel = viewModel
        
        rootNavigationController.pushViewController(viewController, animated: true)
        return viewModel.notesDetailsResult
    }
    
    private func showCreateNote() -> Observable<CreateNoteResult> {
        let viewModel = CreateNoteViewModel(dependencies: dependencies)
        let viewController = UIStoryboard.main.createNoteViewController
        viewController.viewModel = viewModel
        
        rootNavigationController.pushViewController(viewController, animated: true)
        return viewModel.createNoteResult
    }
    
    deinit {
        plog(NotesCoordinator.self)
    }
}
