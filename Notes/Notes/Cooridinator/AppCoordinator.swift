//
//  AppCoordinator.swift
//  Notes
//
//  Created by Prathvi Raj on 14/04/18.
//  Copyright © 2018 tt2k. All rights reserved.
//

import UIKit
import RxSwift

final class AppCoordinator: Coordinator<Void> {
    
    private let navigationController:UINavigationController
    private let window: UIWindow
    private let dependencies: AppDependency
    
    init(window:UIWindow, navigationController:UINavigationController) {
        self.window = window
        self.navigationController = navigationController
        self.dependencies = AppDependency(window: self.window)
    }
    
    override func start() -> Observable<Void> {
        return showNotes()
    }

    private func showNotes() -> Observable<Void> {
        let coordinator = NotesCoordinator(navigationController: navigationController, dependencies: self.dependencies)
        return coordinate(to: coordinator)
    }
    
    deinit {
        plog(AppCoordinator.self)
    }
}
